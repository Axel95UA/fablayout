package com.noisyminer.fablayoutdemo

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.util.Log
import com.noisyminer.fablayout.adapter.FabAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val first = fabLayout.FabItemBuilder("First").apply {
            circleColor = Color.BLACK
            circleDrawable = bindDrawable(R.drawable.ic_sender)
        }.build()

        val second = fabLayout.FabItemBuilder("Second").apply {
            circleSize = dpToPx(16)
        }.build()

        val third = fabLayout.FabItemBuilder("Third").apply {
            titleColor = Color.BLACK
            titleBackgroundColor = Color.WHITE
            titleSize = dpToPx(8).toFloat()
            onClickListener = {
                Log.d("mytg", "Click")
            }
        }.build()

        val hide = fabLayout.FabItemBuilder("Hide").apply {
            onClickListener = {
                fabLayout.hideButton()
            }
        }.build()

        val normal = fabLayout.FabItemBuilder("Normal").build()

        val list = listOf(first, second, third, hide, normal)
        fabLayout.adapter = object : FabAdapter {

            override fun getSize() = list.size

            override fun getFab(pos: Int) = list[pos]
        }

        show.setOnClickListener { fabLayout.showButton() }
    }


    fun dpToPx(dp: Int): Int = dpToPx(dp.toFloat())

    fun dpToPx(dp: Float): Int = Math.round(dp * applicationContext.resources.displayMetrics.density)

    fun bindDrawable(@DrawableRes res: Int): Drawable = ContextCompat.getDrawable(applicationContext, res)
        ?: ShapeDrawable()
}
