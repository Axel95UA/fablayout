## FabLayout

###How to import?

1. Root level

```groovy
allprojects {
    repositories {
    ...
    maven {
        url 'https://jitpack.io'
        credentials { username 'jp_7r2folubajc8v60rj90nrhsc83' }
    }
    ...
```

2. App level

```groovy
implementation 'org.bitbucket.NikitaGordiaNoisy:fablayout:1.1.4'
```

###How to use it?

* Create item:
	1. Create instance of ```fabLayout.FabItemBuilder``` with ```title``` parameter. E. g. ```fabLayout.FabItemBuilder("First")```
	2. Extend with ```.apply {}``` and change parameters:

		| Target      | Property                    |
		|:------------|:---------------------------|
		| item circle | circleColor                 |
		| item circle | circleDrawable              |
		| item circle | circleSize                  |
		| item title  | titleColor                  |
		| item title  | titleSize                   |
		| item title  | titleBackgroundColor        |
		| both        | onClickListener             |
	3. Build item with ```build()```

	E. g.

```java
       val third = fabLayout.FabItemBuilder("Third").apply {
            titleColor = Color.BLACK
            titleBackgroundColor = Color.WHITE
            titleSize = dpToPx(8).toFloat()
            onClickListener = {
                Log.d("mytg", "Click")
            }
        }.build()
```

* Create adater.
	1. Implement ```FabAdapter```
	2. Set ```fabAdapter.adapter``` instance of implementation

	E. g.

```java
        val list = listOf(first, second, third, normal)
        fabLayout.adapter = object : FabAdapter {

            override fun getSize() = list.size

            override fun getFab(pos: Int) = list[pos]
        }
```

* Use this layout as container for your views

```xml
	<com.noisyminer.fablayout.FabLayout
	    android:id="@+id/fabLayout"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent"
	    app:button_color="@color/colorPrimaryDark"
	    app:button_size="56dp">
	    
	    ...Your views
	    
	</com.noisyminer.fablayout.FabLayout>
```

The following attributes is available:

```xml
	<attr name="button_color" format="color"/>
	<attr name="button_size" format="dimension"/>
	<attr name="button_margin" format="dimension"/>
	<attr name="dim_alpha" format="float"/>
	<attr name="image_padding" format="dimension"/>
	<attr name="item_elevation" format="dimension"/>
	<attr name="item_title_corners" format="dimension"/>
	<attr name="item_title_padding" format="dimension"/>
	<attr name="item_image_padding" format="dimension"/>
	<attr name="item_animation_scale" format="float"/>
```

* (Optional) Also you can use the following methods/properties:
 
	| Method/Property      | Action                    |
	|:---------------------|:---------------------------|
	| expand() | expand fabs                 |
	| collapse() | collapse fabs              |
	| getButtonFab()  | returns button ImageView      |
	| hideButton()  | hides main fab (button)      |
	| showButton()  | shows main fab (button)      |
	| button | main fab ImageView |
	| adaptor | provides information about fabs |
	| onClickListener | handle clicked item position |
	| itemAnimationDuration  | duration of every single item animation                  |
	| animationDuration  | duration of whole animation (dimming, button rotation)                   |
