package com.noisyminer.fablayout

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import android.os.Handler
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewPropertyAnimator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.noisyminer.fablayout.adapter.FabAdapter
import com.noisyminer.fablayout.adapter.FabItem
import com.noisyminer.fablayout.extension.bag
import com.noisyminer.fablayout.model.ItemView

class FabLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
        FrameLayout(context, attrs, defStyle) {

    companion object {

        private val STATE_EXPANDED = 2
        private val STATE_COLLAPSED = 3
        private val STATE_EXPANDING = 4
        private val STATE_COLLAPSING = 5
    }

    //Button
    private val DEFAULT_BUTTON_COLOR = Color.RED
    private val DEFAULT_BUTTON_SIZE = dpToPx(56F)
    private val DEFAULT_BUTTON_MARGIN = dpToPx(16F)
    private val DEFAULT_BUTTON_IMAGE_PADDING = dpToPx(14F)
    private val DEFAULT_BUTTON_ELEVATION = dpToPx(2F)

    //Dim state
    private val DEFAULT_DIM_ALPHA = 0.4F

    //Item
    private val DEFAULT_ITEM_TITLE_CORNER = dpToPx(6F)
    private val DEFAULT_ITEM_TITLE_PADDING = dpToPx(4F)
    private val DEFAULT_ITEM_IMAGE_PADDING = dpToPx(10F)
    private val DEFAULT_ITEM_ANIMATION_SCALE = 0.8F
    private val DEFAULT_CIRCLE_COLOR = Color.WHITE
    private val DEFAULT_CIRCLE_DRAWABLE = null
    private val DEFAULT_CIRCLE_SIZE = dpToPx(40)
    private val DEFAULT_TITLE_COLOR = Color.WHITE
    private val DEFAULT_TITLE_BACKGROUND_COLOR = Color.GRAY
    private val DEFAULT_TITLE_SIZE = dpToPx(5).toFloat()

    private val style = getContext().theme.obtainStyledAttributes(attrs, R.styleable.FabLayout, 0, 0)

    private val buttonColor = style.getColor(R.styleable.FabLayout_button_color, DEFAULT_BUTTON_COLOR)
    private val buttonSize = style.getDimensionPixelSize(R.styleable.FabLayout_button_size, DEFAULT_BUTTON_SIZE)
    private val buttonMargin = style.getDimensionPixelOffset(R.styleable.FabLayout_button_margin, DEFAULT_BUTTON_MARGIN)
    private val dimAlpha = style.getFloat(R.styleable.FabLayout_dim_alpha, DEFAULT_DIM_ALPHA)
    private val dimColor = style.getColor(R.styleable.FabLayout_dim_color, Color.BLACK)
    private val imagePadding =
            style.getDimensionPixelOffset(R.styleable.FabLayout_image_padding, DEFAULT_BUTTON_IMAGE_PADDING)
    private val itemElevation =
            style.getDimensionPixelOffset(R.styleable.FabLayout_item_elevation, DEFAULT_BUTTON_ELEVATION).toFloat()
    private val itemTitleCorner =
            style.getDimensionPixelSize(R.styleable.FabLayout_item_title_corners, DEFAULT_ITEM_TITLE_CORNER).toFloat()
    private val itemTitlePadding =
            style.getDimensionPixelOffset(R.styleable.FabLayout_item_title_padding, DEFAULT_ITEM_TITLE_PADDING)
    private val itemImagePadding =
            style.getDimensionPixelOffset(R.styleable.FabLayout_item_image_padding, DEFAULT_ITEM_IMAGE_PADDING)
    private val itemAnimationScale =
            style.getFloat(R.styleable.FabLayout_item_animation_scale, DEFAULT_ITEM_ANIMATION_SCALE)

    private var state = STATE_COLLAPSED

    var animationDuration = 300L
    val itemAnimationDuration = 100L
    var adapter: FabAdapter? = null
    var onClickListener: ((Int) -> Unit)? = null

    private val animationHandler = Handler()
    private val animBag = mutableListOf<ViewPropertyAnimator>()
    private var itemAnimations = listOf<DelayedAnimationRunnable>()

    private val container = LinearLayout(context).apply {
        layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        orientation = LinearLayout.VERTICAL
        isClickable = true
    }

    private val dimView = View(context).apply {
        layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        setBackgroundColor(dimColor)
        alpha = 0F
        setOnClickListener { if (state == STATE_EXPANDED) collapse() }
        isClickable = false
    }

    val button = ImageView(context).apply {
        layoutParams = FrameLayout.LayoutParams(buttonSize, buttonSize).apply {
            gravity = Gravity.BOTTOM or Gravity.END
            setMargins(buttonMargin, buttonMargin, buttonMargin, buttonMargin)
        }
        background = ShapeDrawable(OvalShape()).apply {
            paint.color = buttonColor
            setPadding(imagePadding, imagePadding, imagePadding, imagePadding)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.elevation = itemElevation
        }
        setImageResource(R.drawable.ic_plus)
        setOnClickListener {
            when (state) {
                STATE_COLLAPSED -> expand()
                STATE_EXPANDED -> collapse()
            }
        }
    }

    private val fabContainer = LinearLayout(context).apply {
        layoutParams =
                FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT)
                        .apply {
                            gravity = Gravity.BOTTOM or Gravity.END
                            setMargins(0, 0, 0, buttonSize + buttonMargin * 2)
                        }
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.BOTTOM
        isClickable = false
    }

    private val fabList = mutableListOf<ItemView>()

    init {
        addView(container)
        addView(dimView)
        addView(button)
        addView(fabContainer)
    }

    fun expand() {
        updateFabs()
        state = STATE_EXPANDING
        button.animate()
                .rotation(135F)
                .setDuration(animationDuration * 3 / 2)
                .bag(animBag)
                .withEndAction {
                    state = STATE_EXPANDED
                    fabContainer.isClickable = true
                }.setInterpolator(DecelerateInterpolator())
        dimView.apply {
            isClickable = true
            animate().alpha(dimAlpha)
                    .bag(animBag)
                    .interpolator = DecelerateInterpolator()
        }
        container.isClickable = false
    }

    fun collapse(selected: Int = -1) {
        collapseFabs(selected)
        fabContainer.isClickable = false
        state = STATE_COLLAPSING
        button.animate()
                .rotation(0F)
                .setDuration(animationDuration)
                .bag(animBag)
                .withEndAction {
                    state = STATE_COLLAPSED
                }.interpolator = DecelerateInterpolator()
        dimView.apply {
            isClickable = false
            animate()
                    .bag(animBag)
                    .alpha(0F).interpolator = DecelerateInterpolator()
        }
        container.isClickable = true
    }

    fun hideButton() {
        button.animate()
            .translationY((buttonMargin + buttonSize).toFloat())
            .setDuration(animationDuration)
            .setInterpolator(AccelerateInterpolator())
            .bag(animBag)
    }

    fun showButton() {
        button.animate()
            .translationY(0F)
            .setDuration(animationDuration)
            .setInterpolator(DecelerateInterpolator())
            .bag(animBag)
    }

    private fun updateFabs() {
        fabContainer.removeAllViews()
        fabList.clear()

        adapter?.let {
            for (i in 0..(it.getSize() - 1))
                fabContainer.addView(inflateFabItem(i, it.getFab(i)))
        }

        expandFabs()
    }

    private fun expandFabs() {
        if (fabList.isEmpty()) return
        var delay = 0L
        val step = (animationDuration - itemAnimationDuration) / fabList.size
        itemAnimations = fabList.reversed().map { DelayedAnimationRunnable(it, true) }
        itemAnimations.forEach {
            animationHandler.postDelayed(it, delay)
            delay += step
        }
    }

    private fun collapseFabs(selected: Int = -1) {
        if (fabList.isEmpty()) return
        var delay = 0L
        val step = (animationDuration - itemAnimationDuration) / fabList.size
        itemAnimations = fabList.map { DelayedAnimationRunnable(it, false) }
        if (selected != -1) animateSelectedItem(fabList[selected])
        itemAnimations.forEachIndexed { index, delayedAnimationRunnable ->
            if (index != selected) animationHandler.postDelayed(delayedAnimationRunnable, delay)
            delay += step
        }
    }

    private fun animateSelectedItem(item: ItemView) {
        item.circle.animate()
                .scaleX(2 - itemAnimationScale)
                .scaleY(2 - itemAnimationScale)
                .setDuration(itemAnimationDuration * 2)
                .bag(animBag)
                .withEndAction {
                    item.circle.animate()
                            .scaleX(itemAnimationScale)
                            .scaleY(itemAnimationScale)
                            .setDuration(itemAnimationDuration * 2)
                            .bag(animBag)
                            .alpha(0F)
                }
        item.title.animate()
                .scaleX(2 - itemAnimationScale)
                .scaleY(2 - itemAnimationScale)
                .bag(animBag)
                .setDuration(itemAnimationDuration * 2)
                .alpha(0F)
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child == container || child == button || child == dimView || child == fabContainer) {
            super.addView(child, index, params)
        } else {
            container.addView(child, index, params)
        }
    }

    private fun inflateFabItem(pos: Int, item: FabItem) = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.END or Gravity.CENTER_VERTICAL
        layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val title = inflateItemTitle(pos, item)
        val circle = inflateItemCircle(pos, item)
        fabList.add(ItemView(title, circle))
        addView(title)
        addView(circle)
    }

    private fun inflateItemTitle(pos: Int, item: FabItem) = TextView(context).apply {
        text = item.title
        textSize = item.titleSize
        alpha = 0F
        scaleX = itemAnimationScale
        scaleY = itemAnimationScale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            elevation = itemElevation
        }
        setTextColor(item.titleColor)
        setOnClickListener {
            collapse(pos)
            onClickListener?.invoke(pos)
            item.onClickListener()
        }
        layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply { setMargins(dpToPx(4), dpToPx(4), dpToPx(4), dpToPx(4)) }
        background = ShapeDrawable(
                RoundRectShape(
                        FloatArray(8) { itemTitleCorner },
                        null,
                        FloatArray(8) { itemTitleCorner })
        ).apply {
            paint.color = item.titleBackgroundColor
            setPadding(itemTitlePadding * 2, itemTitlePadding, itemTitlePadding * 2, itemTitlePadding)
        }
    }

    private fun inflateItemCircle(pos: Int, item: FabItem) = ImageView(context).apply {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            elevation = itemElevation
        }
        val margin = buttonMargin + (buttonSize - item.circleSize) / 2
        alpha = 0F
        setOnClickListener {
            collapse(pos)
            onClickListener?.invoke(pos)
            item.onClickListener()
        }
        translationX = (margin + item.circleSize / 2).toFloat()
        layoutParams = LinearLayout.LayoutParams(item.circleSize, item.circleSize).apply {
            setMargins(dpToPx(16), dpToPx(8), margin, dpToPx(8))
        }
        item.circleDrawable?.let { setImageDrawable(it) }
        background = ShapeDrawable(OvalShape()).apply {
            paint.color = item.circleColor
            setPadding(itemImagePadding, itemImagePadding, itemImagePadding, itemImagePadding)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        itemAnimations.forEach { animationHandler.removeCallbacks(it) }
        animBag.forEach { it.cancel() }
    }

    private fun dpToPx(dp: Int): Int = dpToPx(dp.toFloat())

    private fun dpToPx(dp: Float): Int = Math.round(dp * context.resources.displayMetrics.density)

    inner class DelayedAnimationRunnable(val item: ItemView, val expand: Boolean) : Runnable {

        override fun run() {
            if (expand) {
                item.circle.animate()
                        .alpha(1F)
                        .translationX(0F)
                        .setInterpolator(DecelerateInterpolator())
                        .bag(animBag)
                        .duration = itemAnimationDuration
                item.title.animate()
                        .alpha(1F)
                        .scaleY(1F)
                        .scaleX(1F)
                        .setInterpolator(AccelerateInterpolator())
                        .bag(animBag)
                        .duration = itemAnimationDuration
            } else {
                val margin = buttonMargin + (buttonSize - item.circle.width) / 2
                item.circle.animate()
                        .alpha(0F)
                        .translationX((margin + item.circle.width / 2).toFloat())
                        .setInterpolator(AccelerateInterpolator())
                        .bag(animBag)
                        .duration = itemAnimationDuration
                item.title.animate()
                        .alpha(0F)
                        .setInterpolator(AccelerateInterpolator())
                        .scaleY(itemAnimationScale)
                        .scaleX(itemAnimationScale)
                        .bag(animBag)
                        .duration = itemAnimationDuration
            }
        }
    }

    inner class FabItemBuilder(val title: String) {

        var circleColor: Int = DEFAULT_CIRCLE_COLOR
        var circleDrawable: Drawable? = DEFAULT_CIRCLE_DRAWABLE
        var circleSize: Int = DEFAULT_CIRCLE_SIZE
        var titleColor: Int = DEFAULT_TITLE_COLOR
        var titleSize: Float = DEFAULT_TITLE_SIZE
        var titleBackgroundColor: Int = DEFAULT_TITLE_BACKGROUND_COLOR
        var onClickListener: () -> Unit = {}

        fun build() = FabItem(
                title,
                circleColor,
                circleDrawable,
                circleSize,
                titleColor,
                titleBackgroundColor,
                titleSize,
                onClickListener
        )
    }
}