package com.noisyminer.fablayout.adapter

import android.graphics.drawable.Drawable

data class FabItem(val title: String,
                   val circleColor: Int,
                   val circleDrawable: Drawable?,
                   val circleSize: Int,
                   val titleColor: Int,
                   val titleBackgroundColor: Int,
                   val titleSize: Float,
                   val onClickListener: () -> Unit)