package com.noisyminer.fablayout.adapter

interface FabAdapter {

    fun getSize(): Int

    fun getFab(pos: Int): FabItem
}