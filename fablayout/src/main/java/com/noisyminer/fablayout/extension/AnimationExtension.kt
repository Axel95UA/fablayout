package com.noisyminer.fablayout.extension

import android.view.ViewPropertyAnimator

fun ViewPropertyAnimator.bag(list: MutableList<ViewPropertyAnimator>) = apply {
    list.add(this)
}