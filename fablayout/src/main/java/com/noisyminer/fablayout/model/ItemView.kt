package com.noisyminer.fablayout.model

import android.view.View

data class ItemView(val title: View, val circle: View)